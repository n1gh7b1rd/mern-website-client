import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import 'sanitize.css/sanitize.css'
import '../../index.css'
import Container from 'react-bootstrap/Container'

const Home = props => (
  <Container>
    <h1>Home</h1>
    <p>Welcome home!</p>
  </Container>
)

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: () => push('/about')
}, dispatch)

export default connect(
  null,
  mapDispatchToProps
)(Home)