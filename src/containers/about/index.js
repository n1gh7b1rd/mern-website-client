import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import 'sanitize.css/sanitize.css'
import '../../index.css'
import Container from 'react-bootstrap/Container'

const About = () => (
  <Container>
    <h1>About Page</h1>
    <p>Did you get here via Redux?</p>
  </Container>
)


const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: () => push('/')
}, dispatch);

export default connect(null, mapDispatchToProps)(About)