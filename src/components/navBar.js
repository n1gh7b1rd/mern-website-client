import React from 'react'
import { Link } from 'react-router-dom'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { push } from 'connected-react-router'

const NavBar = (props) => {
    console.log(props.pathname);
    return(<div>
        <div>
            {/* <Link to="/">Home</Link>
            <Link to="/about">Hello</Link> */}
            <Breadcrumb>
                <Breadcrumb.Item onClick={() => props.goToHome()} active={props.pathname == '/'}>Home</Breadcrumb.Item>
                <Breadcrumb.Item onClick={() => props.goToAbout()} active={props.pathname == '/about'}>About</Breadcrumb.Item>
            </Breadcrumb>;
        </div>
    </div>)
}

const mapStateToProps = state => ({
    pathname: state.router.location.pathname,
    search: state.router.location.search,
    hash: state.router.location.hash,
  })

const mapDispatchToProps = dispatch => bindActionCreators({
    goToHome: () => push('/'),
    goToAbout: () => push('/about')
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NavBar)