import { Provider } from 'react-redux'
import React from 'react';
import { Route, Switch } from 'react-router' // react-router v4/v5
import { ConnectedRouter } from 'connected-react-router'
import configureStore, { history } from './configureStore'
import Home from './containers/home'
import About from './containers/about'
import ReactDOM from 'react-dom';
import NavBar from './components/navBar'


const store = configureStore();

const target = document.querySelector('#root')

ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
          <link rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"/>
          <NavBar/>
          <Switch>
            <Route exact path="/" render={() => (<Home/>)} />
            <Route path ="/about" render={() => (<About/>)} />
          </Switch>
      </ConnectedRouter>
    </Provider>,
    target
  )